<?php
/**
 * Created by PhpStorm.
 * User: nguye
 * Date: 25/10/2018
 * Time: 23:51 PM
 */
/*
 Template Name: general
 */
get_header();
?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/assets/css/general.css' ?>">
<div id="fullPageGeneral" data-page="<?php echo get_the_ID() ?>">
    <div class="bannerPage" style='background-image: url("<?php echo get_the_post_thumbnail_url() ?>")'>
    </div>
    <div class="containerPage">
        <div class="wrapPage">
            <div class="contentPage">
                <div class="wrapContentPage">
                    <?php while (have_posts()) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
                        <div class="entry-content-page">
                            <?php the_content(); ?> <!-- Page Content -->
                        </div><!-- .entry-content-page -->
                    <?php
                    endwhile; //resetting the page loop
                    wp_reset_query(); //resetting the page query
                    ?>
                </div>
            </div>
            <div class="fullField">
                <div class="wrapField">
                    <?php
                    $list_field = ['field-1', 'field-2'];
                    foreach ($list_field as $item_field) {
                        $field = get_field($item_field);
                        if ($field) { ?>
                            <div class="wrapProcess ">
                                <?php if ($field['title-field']) { ?>
                                    <h3 class="titleField">
                                        <span><?php echo $field['title-field'] ?></span>
                                        <i class="flaticon-plus"></i>
                                    </h3>
                                <?php } ?>
                                <?php if ($field['content-field']) { ?>
                                    <div class="contentField">
                                        <?php echo $field['content-field']; ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <?php
                        }
                    } ?>
                </div>
            </div>

        </div>
    </div>

    <?php
    $id_page = get_the_ID();
    //Get the images ids from the post_metadata
    $per_page = 12;
    $images = acf_photo_gallery('gallery', $id_page);
    $countGallery = count($images);

    $total_page = ceil($countGallery / $per_page);
    //Check if return array has anything in it
    if ($countGallery):
        ?>

        <div id="galleryPage"  class="galleryPage">
            <?php
            //Title gallery
            if (get_field('title_gallery')) { ?>
                <h2 id="title-gallery"><?php the_field('title_gallery') ?></h2>
            <?php } ?>
            <div class="wrapGalleryPage row">
                <?php
                foreach ($images as $key => $image):

                    $id = $image['id']; // The attachment id of the media
                    $title = $image['title']; //The title
                    $caption = $image['caption']; //The caption
                    $full_image_url = $image['full_image_url']; //Full size image url
                    $resize_image_url = acf_photo_gallery_resize_image($full_image_url, 262, 160); //Resized size to 262px width by 160px height image url
                    $thumbnail_image_url = $image['thumbnail_image_url']; //Get the thumbnail size image url 150px by 150px
                    $url = $image['url']; //Goto any link when clicked
                    $target = $image['target']; //Open normal or new tab
                    $alt = get_field('photo_gallery_alt', $id); //Get the alt which is a extra field (See below how to add extra fields)
                    $class = get_field('photo_gallery_class', $id); //Get the class which is a extra field (See below how to add extra fields)

                    ?>

                    <div class="thumbnail col-xl-3 col-md-4 col-sm-6 col-12 "
                         style="display: <?php echo ($key < $per_page) ? 'block' : 'none' ?>">
                        <div class="wrapThumbnail">
                            <?php if (!empty($url)) { ?><a
                                    href="<?php echo $url; ?>" <?php echo ($target == 'true') ? 'target="_blank"' : ''; ?>><?php } ?>
                                <img src="<?php echo $resize_image_url; ?>" alt="<?php echo $title; ?>"
                                     title="<?php echo $title; ?>">
                                <?php
                                $data_popup = [
                                    'id' => $id_page,
                                    'key' => $key,
                                    'name_image' => $title,
                                    'src' => $full_image_url,
                                    'name' => 'gallery',
                                    'total_el' => $countGallery,
                                ];
                                $data_popup = json_encode($data_popup);
                                ?>
                                <span onclick='sildeGallery.popup_gallery(<?php echo $data_popup ?>,event)'
                                      class="flaticon-plus"></span>
                                <?php if (!empty($url)) { ?></a><?php } ?>
                        </div>
                        <span title="<?php echo $title; ?>" class="title-image"><?php echo $title; ?></span>
                    </div>
                <?php endforeach; ?>
            </div>
            <div id="pagination-gallery">
                <?php echo do_shortcode("[linh_pagination total_pages=" . $total_page . " current_page=1 text_prev='prev' text_next='next']"); ?>
            </div>
        </div>
    <?php endif; ?>


    <?php
    //Get the images ids from the post_metadata
    $per_page_2 = 12;
    $images_2 = acf_photo_gallery('sample_class', $id_page);
    $countGallery_2 = count($images_2);
    $total_page_2 = ceil($countGallery_2 / $per_page_2);
    //Check if return array has anything in it
    if ($countGallery_2):?>

        <div id="galleryPage2" class="galleryPage">
            <?php
            //Title sample slabs
            if (get_field('title_sample_slabs')) { ?>
                <h2 id="title-sample-slabs"><?php the_field('title_sample_slabs') ?></h2>
            <?php } ?>
            <div class="wrapGalleryPage row">
                <?php
                foreach ($images_2 as $key_2 => $image_2):
                    $id_2 = $image_2['id']; // The attachment id of the media
                    $title_2 = $image_2['title']; //The title
                    $caption_2 = $image_2['caption']; //The caption
                    $full_image_url_2 = $image_2['full_image_url']; //Full size image url
                    $resize_image_url_2 = acf_photo_gallery_resize_image($full_image_url_2, 262, 160); //Resized size to 262px width by 160px height image url
                    $thumbnail_image_url_2 = $image_2['thumbnail_image_url']; //Get the thumbnail size image url 150px by 150px
                    $url_2 = $image_2['url']; //Goto any link when clicked
                    $target_2 = $image_2['target']; //Open normal or new tab
                    $alt_2 = get_field('photo_gallery_alt', $id_2); //Get the alt which is a extra field (See below how to add extra fields)
                    $class_2 = get_field('photo_gallery_class', $id_2); //Get the class which is a extra field (See below how to add extra fields)
                    ?>
                    <div class="thumbnail col-xl-3 col-md-4 col-sm-6 col-12 "
                         style="display: <?php echo ($key_2 < $per_page_2) ? 'block' : 'none' ?>">
                        <div class="wrapThumbnail">
                            <?php if (!empty($url_2)) { ?><a
                                    href="<?php echo $url_2; ?>" <?php echo ($target_2 == 'true') ? 'target="_blank"' : ''; ?>><?php } ?>
                                <img src="<?php echo $resize_image_url_2; ?>" alt="<?php echo $title_2; ?>"
                                     title="<?php echo $title_2; ?>">
                                <?php
                                $data_popup_2 = [
                                    'id' => $id_page,
                                    'key' => $key_2,
                                    'name_image' => $title_2,
                                    'src' => $full_image_url_2,
                                    'name' => 'sample_class',
                                    'total_el' => $countGallery_2
                                ];
                                $data_popup_2 = json_encode($data_popup_2);
                                ?>
                                <span onclick='sildeGallery.popup_gallery(<?php echo $data_popup_2 ?>,event)'
                                      class="flaticon-plus"></span>
                                <?php if (!empty($url_2)) { ?></a><?php } ?>
                        </div>
                        <span title="<?php echo $title_2; ?>" class="title-image"><?php echo $title_2; ?></span>
                    </div>
                <?php endforeach; ?>
            </div>
            <div id="pagination-gallery-2">
                <?php echo do_shortcode("[linh_pagination total_pages=" . $total_page . " current_page=1 text_prev='prev' text_next='next']"); ?>
            </div>
        </div>
    <?php endif; ?>

    <div id="popupGallery">
        <div class="fixedPopupGallery"></div>
        <div class="innerPopupGalley">
            <div class="div"></div>
            <div class="wrapPopupGallery" onclick="close_popup_gallery(event)">
                <div class="thumbnail"></div>
            </div>
            <span onclick="sildeGallery.prev_gallery()" class="prev flaticon-arrowhead-thin-outline-to-the-left"></span>
            <span onclick="sildeGallery.next_gallery()" class="next flaticon-arrow"></span>
        </div>
    </div>
</div>
<script src="<?php echo get_template_directory_uri() . '/assets/js/config_ajax.js' ?>"></script>
<script>
    var data = {
        el_click: '#pagination-gallery li a',
        el_li: '#pagination-gallery li',
        el_thumb: '#galleryPage .wrapGalleryPage .thumbnail',
        wrap_gallery: '#galleryPage .wrapGalleryPage',
        wrap_pagination: '#pagination-gallery'
    };
    pagination_page(<?php echo get_the_ID() ?>, 'gallery',<?php echo $total_page?>, data)

    var data_2 = {
        el_click: '#pagination-gallery-2 li a',
        el_li: '#pagination-gallery-2 li',
        el_thumb: '#galleryPage2 .wrapGalleryPage .thumbnail',
        wrap_gallery: '#galleryPage2 .wrapGalleryPage',
        wrap_pagination: '#pagination-gallery-2'
    };
    pagination_page(<?php echo get_the_ID() ?>, 'sample_class',<?php echo $total_page_2 ?>, data_2)
</script>
<?php get_footer(); ?>

