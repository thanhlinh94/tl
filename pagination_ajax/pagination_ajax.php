<?php
/**
 * Created by PhpStorm.
 * User: nguye
 * Date: 27/10/2018
 * Time: 22:44 PM
 */


/******************
 * Function phân trang PHP có dạng 1,2,3 ...
 ********************/
function paginate_function($atts)
{
    $atts = shortcode_atts(
        array(
            'current_page' => 1,
            'total_pages' => 0,
            'text_prev' => '&lt;',
            'text_next' => '&gt;',
            'class_next' => '',
            'class_prev' => '',
        ), $atts);

    $current_page = $atts['current_page'];
    $total_pages = $atts['total_pages'];

    $pagination = '';
    $pagination .= '
    <style type="text/css">
    .pagination-linh{
    list-style: none;
    display: flex;
    justify-content: center;
    padding: 0;
    border-top: 1px solid #e2e2e2;
    padding: 10px 0 ;
    margin: 0;
    }
    .pagination-linh li{
        text-transform: uppercase;
    font-size: 17px;
    font-weight: bold;
    padding: 10px;
    }
    .pagination-linh li a{
    color: #999;
    }
     .pagination-linh li.active a {
    color: #595959;
    }
</style>
    ';
    if ($total_pages > 0 && $total_pages != 1 && $current_page <= $total_pages) { //verify total pages and current page number
        $pagination .= '<ul class="pagination-linh">';
        $right_links = $current_page + 3;
        $previous = $current_page - 1; //previous link
        $next = $current_page + 1; //next link
        $first_link = true; //boolean var to decide our first link

        if ($current_page > 1) {
            $previous_link = ($previous == 0) ? 1 : $previous;
//            $pagination .= '<li class="first"><a href="#" data-page="1" title="First">&laquo;</a></li>'; //first link
            $pagination .= '<li><a href="#" class="' . $atts['class_prev'] . '" data-page="' . $previous_link . '" title="Previous">' . $atts['text_prev'] . '</a></li>'; //previous link
            for ($i = ($current_page - 2); $i < $current_page; $i++) { //Create left-hand side links
                if ($i > 0) {
                    $pagination .= '<li><a href="#" data-page="' . $i . '" title="Page' . $i . '">' . $i . '</a></li>';
                }
            }
            $first_link = false; //set first link to false
        }

        if ($first_link) { //if current active page is first link
            $pagination .= '<li class="first active"><a href="#" data-page="' . $current_page . '" title="Page' . $current_page . '">' . $current_page . '</a></li>';
        } elseif ($current_page == $total_pages) { //if it's the last active link
            $pagination .= '<li class="last active"><a href="#" data-page="' . $current_page . '" title="Page' . $current_page . '">' . $current_page . '</a></li>';
        } else { //regular current link
            $pagination .= '<li class="active"><a href="#" data-page="' . $current_page . '" title="Page' . $current_page . '">' . $current_page . '</a></li>';
        }

        for ($i = $current_page + 1; $i < $right_links; $i++) { //create right-hand side links
            if ($i <= $total_pages) {
                $pagination .= '<li><a href="#" data-page="' . $i . '" title="Page ' . $i . '">' . $i . '</a></li>';
            }
        }
        if ($current_page < $total_pages) {
            $next_link = ($next > $total_pages) ? $total_pages : $next;
            $pagination .= '<li><a href="#" class="' . $atts['class_next'] . '" data-page="' . $next_link . '" title="Next">' . $atts['text_next'] . '</a></li>'; //next link
//            $pagination .= '<li class="last"><a href="#" data-page="'.$total_pages.'" title="Last">&raquo;</a></li>'; //last link
        }

        $pagination .= '</ul>';
    }
    return $pagination; //return pagination links
}

add_shortcode('linh_pagination', 'paginate_function');