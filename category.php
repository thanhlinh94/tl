<?php 
/*
 Template Name: category
 */
get_header();
 ?>
<style type="text/css">
	.au-bg-head {
		padding-top: 96px;
	}
	@media only screen and (max-width: 900px) {
	    .au-bg-head {
	        padding-top: 229px;
	    }
	}
	.au-gb-section {
		background-color: #fff;
		position: relative;
	}
	.au-bg-section-1 {
	    padding-top: 15px 0 2px;
	}
	.au-bg-title {
		font-size: 50px;
		color: #555;
		font-weight: 700;
		margin: 45px 0 45px 0;
	}
	.au-bg-title.tt-bl {
		text-align: center;
	}
	.au-gb-section-2 {
	    padding-top: 15px;
	    padding-right: 0;
	    padding-bottom: 12px;
	    padding-left: 0;
	}
	.au-bg-hr {
		height: 23px;
	}
	.au-divider {
	    margin: 0;
	    padding: 0;
	    border: 0;
	    outline: 0;
	    background: 0 0;
	    font-size: 100%;
	    vertical-align: baseline;
	}
	.au-bg-divider {
		display: inline-block;
		width: 100%;
	}
	.au-bg-line {
		position: relative;
	}
	.au-bg-line:before {
		position: absolute;
	    z-index: 10;
	    top: 0;
	    left: 0;
	    width: 100%;
	    height: 1px;
	    border-top-width: 1px;
	    border-top-style: solid;
	    border-top-color: #eee;
	    border-top-color: rgba(0,0,0,.1);
	    content: "";
	}
	.au-bg-line:after {
	    display: table;
    	content: "";
	}
	.au-gb-section-2 {
		padding: 27px 0;
	}
	.au-wrap-pic {
		display: inline-flex;
		justify-content: center;
	}
	.au-cl-1-2 .au-cl-1 {
		display: flex;
		flex-direction: column;
	}
	.au-pic {
		display: flex;
		justify-content: center;
		align-items: center;
		vertical-align: middle;
		margin-bottom: 15px;
	}
	.au-wrap-pic img {
		width: 100%;
		height: 100%;
	}
	.au-txt {
		font-size: 20px;
		font-weight: 400;
		color: #555;
		line-height: 1.5em;
	}
	.au-cap-txt {
		font-style: italic;
	}
	.au-pic-cl {
		margin-bottom: 15px;
	}
	.au-bg-btn {
		padding: 27px 0 50px;
		display: flex;
		justify-content: center;
		align-items: center;
	}
	.au-txt-href {
		color: #fff;
		background-color: #8e8e8e;
		font-weight: 700;
		font-size: 20px;
		transition: all 0.3s;
	    padding: 10px 20px;
    	line-height: 30px;
		text-decoration: none;
		text-transform: uppercase;
	}
	.au-txt-href:hover {
		background-color: #595959;
	}
	.navigation.pagination {
	    width: 100%;
		border-top: 1px solid #e2e2e2;
		position: relative;
	}
	.nav-links {
		text-align: center;
	}
	.navigation.pagination .page-numbers {
		padding: 10px;
		line-height: 26px;
		font-size: 20px;
		font-weight: 800;
		color: #999;
	}
	.screen-reader-text {
		margin: 0;
	}
	.navigation.pagination .page-numbers.current {
		color: #595959;
	}
</style>
<div class="au-full">
	<div class="au-bg-head au-img-bg"></div>
	<div class="container">
		<div class="au-gb-section au-bg-section-1">
			<h1 class="au-bg-title tt-bl"><?php single_cat_title(); ?></h1>
		</div>
		<?php if ( have_posts() ) : ?>
		<div class="au-bg-hr au-bg-line">
			<div class="au-bg-divider au-divider"></div>
		</div>
		<div class="au-gb-section au-gb-section-2">
			<div class="row">
				<?php 
				while ( have_posts() ) : the_post();
				 ?>
				<div class="col-md-6">
					<div class="au-pic-cl">
						<div class="au-pic">
							<a class="au-wrap-pic" href="<?php the_permalink() ?>">
								<?php the_post_thumbnail( 'full' ); ?>
							</a>
						</div>
						<div class="au-pic-cap">
							<a class="au-cap-txt au-txt" href="<?php the_permalink() ?>"><?php the_title(); ?></a>
						</div>
					</div>
				</div>
				<?php endwhile; ?>
				
			</div>
			<div class="row">
				<?php 
					the_posts_pagination( array(
						'prev_text'          => __( 'Previous page', 'twentysixteen' ),
						'next_text'          => __( 'Next page', 'twentysixteen' ),
						 'screen_reader_text'=> '&nbsp;'
					) );
				 ?>
			</div>
		</div>
		<?php endif; ?>
	</div>
</div>
 <?php 
get_footer();
 ?>