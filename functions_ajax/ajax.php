<?php
/**
 * Created by PhpStorm.
 * User: nguye
 * Date: 28/10/2018
 * Time: 1:25 AM
 */

/** Xử lý Ajax trong WordPress */
add_action('wp_ajax_pagination_gallery', 'pagination_gallery');
add_action('wp_ajax_nopriv_pagination_gallery', 'pagination_gallery');
function pagination_gallery()
{
    $return = ['gallery' => "", 'pagination' => ""];
    try {
        $page = $_POST['page_select'] ? intval($_POST['page_select']) : 0;
        $total_page = $_POST['total_page'] ? intval($_POST['total_page']) : 0;
        $return['pagination'] = do_shortcode("[linh_pagination total_pages=" . $total_page . " current_page=" . $page . " text_prev='prev' text_next='next']");
        wp_send_json_success($return);
    } catch (\Exception $exception) {
        wp_send_json_error(['msg' => $exception]);
    }
    wp_die();
}

add_action('wp_ajax_popup_gallery', 'popup_gallery');
add_action('wp_ajax_nopriv_popup_gallery', 'popup_gallery');

function popup_gallery()
{
    $return = ['gallery' => ""];
    try {
        $id = $_POST['id_page'] ? intval($_POST['id_page']) : 0;
        $name_gallery = $_POST['name_gallery'] ? $_POST['name_gallery'] : '';
        $images = acf_photo_gallery($name_gallery, $id);
        $countGallery = count($images);
        if ($countGallery) {
            $return['gallery'] = $images;
            $return['total_el'] = $countGallery;
        }

        wp_send_json_success($return);
    } catch (\Exception $exception) {
        wp_send_json_error(['msg' => $exception]);
    }

    wp_die();
}

add_action('wp_ajax_popup_video_home', 'popup_video_home');
add_action('wp_ajax_nopriv_popup_video_home', 'popup_video_home');

function popup_video_home()
{
    $return = ['video' => ""];
    try {
        $id = $_POST['id'] ? intval($_POST['id']) : '';
        $list_slide = ['item_slide_1', 'item_slide_2', 'item_slide_3', 'item_slide_4'];
        $list_video = [];
        if($id){
            foreach ($list_slide as $key=>$value){
                $item = (get_field($value,$id));
                array_push($list_video,$item['video']);
            }
            $return['video'] = $list_video;
        }
        wp_send_json_success($return);
    } catch (\Exception $exception) {
        wp_send_json_error(['msg' => $exception]);
    }

    wp_die();
}