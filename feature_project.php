<?php
/*
 Template Name: feature-project
 */
get_header();
?>
    <style type="text/css">
        <?php if (has_post_thumbnail( $post->ID ) ): ?>
        <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail'); ?>
        <?php endif; ?>
        .fea-img-bg {
            background-image: url('<?php echo $image[0]; ?>');
            background-position: center left;
            background-size: cover;
        }

        .fea-bg-head {
            padding-top: 96px;
            padding-bottom: 150px;
        }

        @media only screen and (max-width: 900px) {
            .au-bg-head {
                padding-top: 81px;
            }
        }

        .fea-content {
            padding: 54px 0;
        }

        .fea-tt-head {
            padding: 27px 0 10px;
            margin: 0;
            font-weight: 700;
            font-style: normal;
            text-transform: none;
            text-decoration: none;
            font-size: 30px;
            color: #555;
            line-height: 1em;
            text-align: center;
        }

        .fea-title-head {
            margin-bottom: 2.75%;
        }

        .fea-txt-content {
            font-size: 20px;
            font-weight: 400;
            color: #555;
        }

        .fea-bg-1 {
            border-top: 1px solid #d8d8d8;
            padding-top: 15px;
            padding-right: 0;
            padding-bottom: 0;
            padding-left: 0;
        }

        .fea-tt-bl {
            font-size: 24px;
            text-transform: uppercase;
            font-weight: 700;
            color: #555;
            text-align: center;
        }

        .fea-tt-bl-1 {
            padding: 27px 0 13px;
            margin: 0;
        }

        .fea-content-bg-1 {
            padding: 15px 0 20px;
        }

        .fea-tt-bl-2 {
            padding-bottom: 10px;
            margin: 0;
        }

        .fea-content-bd-2 {
            margin-top: 2.75%;
        }

        /*
        css gallery
         */
        #galleryPage, #galleryPage2, #galleryPage3 {
            padding: 0 40px;
        }

        #galleryPage2 {
            background-color: #f4f4f4;
        }

        #title-gallery, #title-sample-slabs, .title_gallery_sample {
            display: block;
            text-transform: uppercase;
            text-align: center;
            font-size: 23px;
            color: #555;
            margin: 0;
            padding: 28px 0;
        }

        .wrapGalleryPage {
            margin: -20px -20px 0 -20px;
            padding-bottom: 20px;
        }

        .wrapGalleryPage .thumbnail {
            padding: 17px
        }

        .wrapGalleryPage .thumbnail img {
            width: 100%;
            height: 100%;
            object-fit: cover;
            object-position: center;
        }

        .wrapGalleryPage .wrapThumbnail {
            border-radius: 8px;
            overflow: hidden;
            position: relative;
        }

        .wrapGalleryPage .wrapThumbnail:hover .flaticon-plus {
            display: flex;
        }

        .wrapGalleryPage .wrapThumbnail:hover .flaticon-plus:before {
            -webkit-animation-name: fadeInUp;
            animation-name: fadeInUp;
            -webkit-animation-duration: 0.5s;
            animation-duration: 0.5s;
            -webkit-animation-fill-mode: both;
            animation-fill-mode: both;
        }

        .wrapGalleryPage .wrapThumbnail .flaticon-plus {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: rgba(255, 255, 255, 0.7);
            z-index: 1;
            display: none;
            align-items: center;
            justify-content: center;
            font-size: 29px;
            color: #595959;
            cursor: pointer;
        }

        .wrapGalleryPage .thumbnail .title-image{
            margin-top: 5px;
            font-size: 16px;
            display: block;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }

        #popupGallery {
            display: none;
        }

        .innerPopupGalley {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            z-index: 1000000;
            background: rgba(11,11,11,0.8);
        }
        .innerPopupGalley .prev,.innerPopupGalley .next{
            position: absolute;
            top: 50%;
            color: #fff;
            font-size: 30px;
            padding: 10px;
            cursor: pointer;
            z-index: 100;
            background: rgba(0,0,0,0.5);
        }
        .innerPopupGalley .next{
            right: 0;
        }
        .innerPopupGalley .prev{
            left: 0;
        }
        .wrapPopupGallery {
            display: flex;
            justify-content: center;
            align-items: center;
            text-align: center;
            height: 100%;
            -webkit-transition: all ease-in 0.7s;
            -moz-transition:  all ease-in 0.7s;
            -ms-transition:  all ease-in 0.7s;
            -o-transition:  all ease-in 0.7s;
        }
        .wrapPopupGallery .thumbnail {
            position: relative;
            display: inline-flex;
            vertical-align: middle;
            margin: 0 auto;
            text-align: left;
            z-index: 11;
            max-width: 100vw;
            /*height: 100%;*/
            align-items: center;
        }

        .wrapPopupGallery .thumbnail img {
            width: 100%;
            height: 100%;
            object-fit: cover;
            max-width: 100vw;
            max-height: calc(100vh - 80px);
            cursor: pointer;
        }
        .wrapPopupGallery .thumbnail span{
            padding-top: 5px;
            position: absolute;
            top: 100%;
            right: 40px;
            color: #fff;
        }
        .wrapPopupGallery .thumbnail .flaticon-delete{
            position: absolute;
            bottom: 100%;
            right: 0;
            padding: 10px;
            color: #fff;
            font-size: 18px;
            cursor: pointer;
        }
        .wrapPopupGallery .thumbnail .index{
            padding-top: 5px;
            position: absolute;
            top: 100%;
            right: 40px;
            color: #fff;
            font-size: 12px;
        }
        .wrapPopupGallery .thumbnail .name{
            padding-top: 5px;
            position: absolute;
            top: 100%;
            left: 0px;
            width: calc(100% - 50px);
            color: #fff;
            font-size: 17px;
            padding-left: 10px;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }
        .activeGallery{

        }

        @-webkit-keyframes fadeInUp {
            0% {
                opacity: 0;
                -webkit-transform: translate3d(0, 50%, 0);
                transform: translate3d(0, 50%, 0);
            }
            100% {
                opacity: 1;
                -webkit-transform: none;
                transform: none;
            }
        }

        @keyframes fadeInUp {
            0% {
                opacity: 0;
                -webkit-transform: translate3d(0, 50%, 0);
                transform: translate3d(0, 50%, 0);
            }
            100% {
                opacity: 1;
                -webkit-transform: none;
                transform: none;
            }
        }
    </style>
    <div class="fea-full">
        <div class="fea-bg-head fea-img-bg"></div>
        <div class="container">
            <div class="fea-content">
                <div class="fea-title-head">
                    <h3 class="fea-tt-head"><?php echo get_the_title($page->ID); ?></h3>
                    <hr>
                    <p>&nbsp;</p>
                </div>
                <div class="fea-body fea-txt-content">
                    <?php echo get_post_field('post_content', $post->ID); ?>
                </div>
            </div>
        </div>
        <?php
        $id_page = get_the_ID();
        //Get the images ids from the post_metadata
        $per_page = 12;
        $images = acf_photo_gallery('gallery', $id_page);
        $countGallery = count($images);

        $total_page = ceil($countGallery / $per_page);
        //Check if return array has anything in it
            ?>

            <div id="galleryPage"  class="galleryPage">
                <?php
                //Title gallery
                if (get_field('title_gallery')) { ?>
                    <h2 id="title-gallery"><?php the_field('title_gallery') ?></h2>
                <?php } ?>
                <?php
                if ($countGallery):
                ?>
                <div class="wrapGalleryPage row">
                    <?php
                    foreach ($images as $key => $image):

                        $id = $image['id']; // The attachment id of the media
                        $title = $image['title']; //The title
                        $caption = $image['caption']; //The caption
                        $full_image_url = $image['full_image_url']; //Full size image url
                        $resize_image_url = acf_photo_gallery_resize_image($full_image_url, 262, 160); //Resized size to 262px width by 160px height image url
                        $thumbnail_image_url = $image['thumbnail_image_url']; //Get the thumbnail size image url 150px by 150px
                        $url = $image['url']; //Goto any link when clicked
                        $target = $image['target']; //Open normal or new tab
                        $alt = get_field('photo_gallery_alt', $id); //Get the alt which is a extra field (See below how to add extra fields)
                        $class = get_field('photo_gallery_class', $id); //Get the class which is a extra field (See below how to add extra fields)

                        ?>

                        <div class="thumbnail col-xl-3 col-md-4 col-sm-6 col-12 "
                             style="display: <?php echo ($key < $per_page) ? 'block' : 'none' ?>">
                            <div class="wrapThumbnail">
                                <?php if (!empty($url)) { ?><a
                                        href="<?php echo $url; ?>" <?php echo ($target == 'true') ? 'target="_blank"' : ''; ?>><?php } ?>
                                    <img src="<?php echo $resize_image_url; ?>" alt="<?php echo $title; ?>"
                                         title="<?php echo $title; ?>">
                                    <?php
                                    $data_popup = [
                                        'id' => $id_page,
                                        'key' => $key,
                                        'src' => $full_image_url,
                                        'name_image' => $title,
                                        'name' => 'gallery',
                                        'total_el' => $countGallery,
                                    ];
                                    $data_popup = json_encode($data_popup);
                                    ?>
                                    <span onclick='sildeGallery.popup_gallery(<?php echo $data_popup ?>,event)'
                                          class="flaticon-plus"></span>
                                    <?php if (!empty($url)) { ?></a><?php } ?>
                            </div>
                            <span title="<?php echo $title; ?>" class="title-image"><?php echo $title; ?></span>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div id="pagination-gallery">
                    <?php echo do_shortcode("[linh_pagination total_pages=" . $total_page . " current_page=1 text_prev='prev' text_next='next']"); ?>
                </div>
                <?php endif; ?>
            </div>

        <div class="fea-bg-1">
            <div class="container">
                <?php if (get_field('content_abow')): ?>
                    <div class="fea-content-bg-1">
                        <div class="fea-content-bd-2 fea-txt-content">
                            <?php echo get_field('content_abow'); ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <?php
        //Get the images ids from the post_metadata
        $per_page_2 = 12;
        $images_2 = acf_photo_gallery('sample_class', $id_page);
        $countGallery_2 = count($images_2);
        $total_page_2 = ceil($countGallery_2 / $per_page_2);
        //Check if return array has anything in it
        ?>
        <div id="galleryPage2">
            <?php
            //Title sample slabs
            if (get_field('title_sample_slabs')) { ?>
                <h2 id="title-sample-slabs"><?php the_field('title_sample_slabs') ?></h2>
            <?php } ?>

            <?php if ($countGallery_2): ?>
                <div class="wrapGalleryPage row">
                    <?php
                    foreach ($images_2 as $key_2 => $image_2):
                        $id_2 = $image_2['id']; // The attachment id of the media
                        $title_2 = $image_2['title']; //The title
                        $caption_2 = $image_2['caption']; //The caption
                        $full_image_url_2 = $image_2['full_image_url']; //Full size image url
                        $resize_image_url_2 = acf_photo_gallery_resize_image($full_image_url_2, 262, 160); //Resized size to 262px width by 160px height image url
                        $thumbnail_image_url_2 = $image_2['thumbnail_image_url']; //Get the thumbnail size image url 150px by 150px
                        $url_2 = $image_2['url']; //Goto any link when clicked
                        $target_2 = $image_2['target']; //Open normal or new tab
                        $alt_2 = get_field('photo_gallery_alt', $id_2); //Get the alt which is a extra field (See below how to add extra fields)
                        $class_2 = get_field('photo_gallery_class', $id_2); //Get the class which is a extra field (See below how to add extra fields)
                        ?>
                        <div class="thumbnail col-xl-3 col-md-4 col-sm-6 col-12 "
                             style="display: <?php echo ($key_2 < $per_page_2) ? 'block' : 'none' ?>">
                            <div class="wrapThumbnail">
                                <?php if (!empty($url_2)) { ?><a
                                        href="<?php echo $url_2; ?>" <?php echo ($target_2 == 'true') ? 'target="_blank"' : ''; ?>><?php } ?>
                                    <img src="<?php echo $resize_image_url_2 ; ?>" alt="<?php echo $title_2; ?>"
                                         title="<?php echo $title_2; ?>">
                                    <?php
                                    $data_popup_2 = [
                                        'id' => $id_page,
                                        'key' => $key_2,
                                        'src' => $full_image_url_2,
                                        'name_image' => $title_2,
                                        'name' => 'sample_class',
                                        'total_el' => $countGallery_2
                                    ];
                                    $data_popup_2 = json_encode($data_popup_2);
                                    ?>
                                    <span onclick='sildeGallery.popup_gallery(<?php echo $data_popup_2 ?>,event)'
                                          class="flaticon-plus"></span>
                                    <?php if (!empty($url_2)) { ?></a><?php } ?>
                            </div>
                            <span title="<?php echo $title_2; ?>" class="title-image"><?php echo $title_2; ?></span>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div id="pagination-gallery-2">
                    <?php echo do_shortcode("[linh_pagination total_pages=" . $total_page . " current_page=1 text_prev='prev' text_next='next']"); ?>
                </div>
            <?php endif; ?>
        </div>

        <div class="fea-bg-1">
            <div class="container">
                <?php if (get_field('content_three')): ?>
                    <div class="fea-content-bg-1">
                        <div class="fea-content-bd-2 fea-txt-content">
                            <?php echo get_field('content_three'); ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>

        <?php
        //Get the images ids from the post_metadata
        $per_page_3 = 12;
        $images_3 = acf_photo_gallery('gallery_3', $id_page);
        $countGallery_3 = count($images_3);
        $total_page_3 = ceil($countGallery_3 / $per_page_3);
        //Check if return array has anything in it
        ?>

        <div id="galleryPage3">
            <?php
            //Title sample slabs
            if (get_field('title_gallery_3')) { ?>
                <h2 class="title_gallery_sample"><?php the_field('title_gallery_3') ?></h2>
            <?php } ?>

            <?php if ($countGallery_3): ?>
                <div class="wrapGalleryPage row">
                    <?php
                    foreach ($images_3 as $key_3 => $image_3):
                        $id_3 = $image_3['id']; // The attachment id of the media
                        $title_3 = $image_3['title']; //The title
                        $caption_3 = $image_3['caption']; //The caption
                        $full_image_url_3 = $image_3['full_image_url']; //Full size image url
                        $resize_image_url_3 = acf_photo_gallery_resize_image($full_image_url_3, 262, 160); //Resized size to 262px width by 160px height image url
                        $thumbnail_image_url_3 = $image_3['thumbnail_image_url']; //Get the thumbnail size image url 150px by 150px
                        $url_3 = $image_3['url']; //Goto any link when clicked
                        $target_3 = $image_3['target']; //Open normal or new tab
                        $alt_3 = get_field('photo_gallery_alt', $id_3); //Get the alt which is a extra field (See below how to add extra fields)
                        $class_3 = get_field('photo_gallery_class', $id_3); //Get the class which is a extra field (See below how to add extra fields)
                        ?>
                        <div class="thumbnail col-xl-3 col-md-4 col-sm-6 col-12 "
                             style="display: <?php echo ($key_3 < $per_page_3) ? 'block' : 'none' ?>">
                            <div class="wrapThumbnail">
                                <?php if (!empty($url_3)) { ?><a
                                        href="<?php echo $url_3; ?>" <?php echo ($target_3 == 'true') ? 'target="_blank"' : ''; ?>><?php } ?>
                                    <img src="<?php echo $resize_image_url_3 ; ?>" alt="<?php echo $title_3; ?>"
                                         title="<?php echo $title_3; ?>">
                                    <?php
                                    $data_popup_3 = [
                                        'id' => $id_page,
                                        'key' => $key_3,
                                        'src' => $full_image_url_3,
                                        'name' => 'gallery_3',
                                        'name_image' => $title_3,
                                        'total_el' => $countGallery_3
                                    ];
                                    $data_popup_3 = json_encode($data_popup_3);
                                    ?>
                                    <span onclick='sildeGallery.popup_gallery(<?php echo $data_popup_3 ?>,event)'
                                          class="flaticon-plus"></span>
                                    <?php if (!empty($url_3)) { ?></a><?php } ?>
                            </div>
                            <span title="<?php echo $title_3; ?>" class="title-image"><?php echo $title_3; ?></span>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div id="pagination-gallery-3">
                    <?php echo do_shortcode("[linh_pagination total_pages=" . $total_page_3 . " current_page=1 text_prev='prev' text_next='next']"); ?>
                </div>
            <?php endif; ?>
        </div>

        <div id="popupGallery">
            <div class="fixedPopupGallery"></div>
            <div class="innerPopupGalley">
                <div class="div"></div>
                <div class="wrapPopupGallery" onclick="close_popup_gallery(event)">
                    <div class="thumbnail"></div>
                </div>
                <span onclick="sildeGallery.prev_gallery()"
                      class="prev flaticon-arrowhead-thin-outline-to-the-left"></span>
                <span onclick="sildeGallery.next_gallery()" class="next flaticon-arrow"></span>
            </div>
        </div>
    </div>
    <script src="<?php echo get_template_directory_uri() . '/assets/js/config_ajax.js' ?>"></script>
    <script>
        var data = {
            el_click: '#pagination-gallery li a',
            el_li: '#pagination-gallery li',
            el_thumb: '#galleryPage .wrapGalleryPage .thumbnail',
            wrap_gallery: '#galleryPage .wrapGalleryPage',
            wrap_pagination: '#pagination-gallery'
        };
        pagination_page(<?php echo get_the_ID() ?>, 'gallery',<?php echo $total_page?>, data)

        var data_2 = {
            el_click: '#pagination-gallery-2 li a',
            el_li: '#pagination-gallery-2 li',
            el_thumb: '#galleryPage2 .wrapGalleryPage .thumbnail',
            wrap_gallery: '#galleryPage2 .wrapGalleryPage',
            wrap_pagination: '#pagination-gallery-2'
        };
        pagination_page(<?php echo get_the_ID() ?>, 'sample_class',<?php echo $total_page_2 ?>, data_2)

        var data_3 = {
            el_click: '#pagination-gallery-3 li a',
            el_li: '#pagination-gallery-3 li',
            el_thumb: '#galleryPage3 .wrapGalleryPage .thumbnail',
            wrap_gallery: '#galleryPage3 .wrapGalleryPage',
            wrap_pagination: '#pagination-gallery-3'
        };
        pagination_page(<?php echo get_the_ID() ?>, 'gallery_3',<?php echo $total_page_3 ?>, data_3)
    </script>
<?php
get_footer()
?>