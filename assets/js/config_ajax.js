/*var data_pagination = {
    el_click: '#pagination-gallery li a',
    wrap_gallery: '#galleryPage .wrapGalleryPage',
    wrap_pagination: '#pagination-gallery'
}*/
// function pagination_page(id_page, name_gallery, per_page,data_pagination = {}) {
//     jQuery(document).on("click",data_pagination.el_click,function () {
//         console.log(data_pagination);
//         event.preventDefault();
//         let page = jQuery(this).attr('data-page');
//         console.log(id_page)
//         console.log(name_gallery);
//         // alert(ajaxurl);
//         /** Ajax Call */
//         jQuery.ajax({
//             url: ajaxurl,
//             type: "POST",
//             // dataType : "text",
//             data: ({
//                 action: 'pagination_gallery',
//                 id_page: id_page,
//                 page_select: page,
//                 name_gallery: name_gallery,
//                 per_page: per_page,
//             }),
//             beforeSend: function () {
//                 // $( '.loading_ajaxp' ).css( 'display','block' );
//             },
//             success: function (res, textStatus, jqXHR) {
//                 if(res.success == true){
//                     if(res.data.gallery){
//                         jQuery( data_pagination.wrap_gallery ).html( res.data.gallery );
//                     }
//                     if(res.data.pagination){
//                         jQuery( data_pagination.wrap_pagination ).html( res.data.pagination );
//                     }
//                 }
//
//             },
//             error: function (jqXHR, textStatus, errorThrown) {
//                 console.log('The following error occured: ' + textStatus, errorThrown);
//             },
//             complete: function (jqXHR, textStatus) {
//             }
//         });
//     })
// }

// function popup_gallery(id_page,index,name_gallery){
//     jQuery(document).ready(function () {
//         console.log(id_page);
//         console.log(index);
//         console.log(name_gallery);
//         jQuery.ajax({
//             url: ajaxurl,
//             type: "POST",
//             // dataType : "text",
//             data: ({
//                 action: 'popup_gallery',
//                 id_page: id_page,
//                 index: index,
//                 name_gallery: name_gallery,
//             }),
//             beforeSend: function () {
//                 // $( '.loading_ajaxp' ).css( 'display','block' );
//             },
//             success: function (res, textStatus, jqXHR) {
//                 if(res.success == true){
//                     if(res.data.gallery){
//                         jQuery("#popupGallery").html( res.data.gallery );
//                     }
//                 }
//
//             },
//             error: function (jqXHR, textStatus, errorThrown) {
//                 console.log('The following error occured: ' + textStatus, errorThrown);
//             },
//             complete: function (jqXHR, textStatus) {
//             }
//         });
//     })
// }


function pagination_page(id_page, name_gallery, total_page, data_pagination = {}) {
    jQuery(document).on("click", data_pagination.el_click, function () {
        event.preventDefault();
        // jQuery(data_pagination.el_li).removeClass('active');
        // jQuery(this).parent('li').addClass('active');
        let page = jQuery(this).attr('data-page') ;
        let start = (page - 1) * 12;
        let end = (page  -1) * 12 + 12;
        let el_gallery = jQuery(data_pagination.el_thumb)
        let top_wrap = jQuery(data_pagination.wrap_gallery).position().top;
        jQuery("html, body").animate({scrollTop: top_wrap}, "slow");
        if (page > 0 && page <= total_page) {
            el_gallery.each(function (index, element) {
                if (index >= start && index < end) {
                    jQuery(this).show();
                } else {
                    jQuery(this).hide();
                }
            });
        }
        /** Ajax Call */
        jQuery.ajax({
            url: ajaxurl,
            type: "POST",
            // dataType : "text",
            data: ({
                action: 'pagination_gallery',
                id_page: id_page,
                page_select: page,
                name_gallery: name_gallery,
                total_page: total_page,
            }),
            beforeSend: function () {
            },
            success: function (res, textStatus, jqXHR) {
                if (res.success == true) {
                    if (res.data.pagination) {
                        jQuery(data_pagination.wrap_pagination).html(res.data.pagination);
                    }
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('The following error occured: ' + textStatus, errorThrown);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    })
}