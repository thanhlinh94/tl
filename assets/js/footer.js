window.onscroll = function() {scrollFunction()};
function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("scrollTop").style.transform = "translateX(0)";
    } else {
        document.getElementById("scrollTop").style.transform = "translateX(60px)";
    }
}
function scrollTo(element, to = 0, duration= 1000) {
	const start = element.scrollTop;
    const change = to - start;
    const increment = 20;
    let currentTime = 0;
    const animateScroll = (() => {
    	currentTime += increment;
     	const val = Math.easeInOutQuad(currentTime, start, change, duration);
    	element.scrollTop = val;
		if (currentTime < duration) {
			setTimeout(animateScroll, increment);
		}
    })
    animateScroll();
}
Math.easeInOutQuad = function (t, b, c, d) {
    t /= d/2;
    if (t < 1) return c/2*t*t + b;
   	t--;
	return -c/2 * (t*(t-2) - 1) + b;
 };
 function topFunction() {
 	scrollTo(document.documentElement);
 }