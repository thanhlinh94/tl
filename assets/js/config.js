jQuery(document).ready(function () {
    jQuery('#menu-toggle').click(function () {
        jQuery('#page,header .site-header-main ').toggleClass('active-menu');
        jQuery(this).hide();
        jQuery('#menu-close').show();
    })
    jQuery('#menu-close').click(function () {
        jQuery('#page,header .site-header-main ').toggleClass('active-menu');
        jQuery(this).hide();
        jQuery('#menu-toggle').show();
    })
    jQuery('.fullField .titleField').click(function (event) {
        let el = jQuery(this);
        el.siblings('.contentField').slideToggle(600);
        setTimeout(function () {
            el.parent('.wrapProcess,.wrapFaqs').toggleClass('active');
        }, 600);

    })
})

// function popup_gallery(data, event) {
//     console.log(event.target.parentNode.getBoundingClientRect().top)
//     let element = event.target.parentNode.getBoundingClientRect();
//     let top = element.top;
//     let left = element.left;
//     let witdh = element.width;
//     let height = element.height;
//     console.log(height);
//
//     console.log(data);
//     // document.getElementsByClassName('wrapPopupGallery').style.position = 'fixed';
//     let css = {
//         "position":"fixed",
//         'top':top+"px",
//         'left':left+"px",
//         'width':witdh+"px",
//         'height':height+"px",
//     };
//     jQuery('.wrapPopupGallery').css(css)
//     let css2 = {
//         'top':"0px",
//         'left':"0px",
//         'width':"100%",
//         'height':"100%",
//     }
//
//     setTimeout(function () {
//         jQuery('.wrapPopupGallery').css(css2)
//     },100)
//     setTimeout(function () {
//         jQuery('.wrapPopupGallery').removeAttr('style');
//     },1000)
//
//
//     let output = '<img src="' + data.src + '">'
//     jQuery('#popupGallery .wrapPopupGallery .thumbnail').html(output);
//     jQuery('#popupGallery').show();
//
// }

function close_popup_gallery(event) {
    console.log(event.target.className);
    if (event.target.className != 'wrapPopupGallery' && event.target.className !='flaticon-delete') {

    } else {
        jQuery('#popupGallery .wrapPopupGallery .thumbnail').html("");
        jQuery('#popupGallery').hide();
    }

}

var sildeGallery = {
    index: 0,
    dataGallery: [],
    total_el: 0,
    popup_gallery(data, event) {
        console.log(event.target.parentNode.getBoundingClientRect().top)
        let element = event.target.parentNode.getBoundingClientRect();
        let top = element.top;
        let left = element.left;
        let witdh = element.width;
        let height = element.height;
        // this.dataGallery = data.data_gallery;
        this.index = data.key
        this.total_el = data.total_el

        console.log(this.index );


        let css = {
            "position": "fixed",
            'top': top + "px",
            'left': left + "px",
            'width': witdh + "px",
            'height': height + "px",
        };
        jQuery('.wrapPopupGallery').css(css)
        let css2 = {
            'top': "0px",
            'left': "0px",
            'width': "100%",
            'height': "100%",
        }

        setTimeout(function () {
            jQuery('.wrapPopupGallery').css(css2)
        }, 100)
        // setTimeout(function () {
        //     jQuery('.wrapPopupGallery').removeAttr('style');
        // }, 1000)
        let page_current = data.key + 1;
        let output = '<img src="' + data.src + '">'
        output += '<i class="flaticon-delete"></i>';
        output += '<span class="index">'+page_current+'/'+data.total_el+'</span>';
        output += '<span class="name">'+data.name_image+'</span>';

        jQuery('#popupGallery .wrapPopupGallery .thumbnail').html(output);
        jQuery('#popupGallery').show();
        let vm = this
        jQuery.ajax({
            url: ajaxurl,
            type: "POST",
            // dataType : "text",
            data: ({
                action: 'popup_gallery',
                id_page: data.id,
                name_gallery: data.name,
            }),
            beforeSend: function () {
                // $( '.loading_ajaxp' ).css( 'display','block' );
            },
            success: function (res, textStatus, jqXHR) {
                if (res.success == true) {
                    if (res.data.gallery) {
                        console.log(res.data.gallery);
                        vm.dataGallery = res.data.gallery;
                    }
                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('The following error occured: ' + textStatus, errorThrown);
            },
            complete: function (jqXHR, textStatus) {
            }
        });


    },
    next_gallery() {
        console.log(this.index )
        console.log(this.total_el )
        if (this.index < this.total_el -1  && this.dataGallery.length > 1) {
            let current_page = this.index + 1;
            let page_current = current_page + 1;
            let link = this.dataGallery[current_page].full_image_url;
            let output = '<img src="' + link + '">'
            output += '<i class="flaticon-delete"></i>';
            output += '<span class="index">'+ page_current  +'/'+this.total_el+'</span>';
            output += '<span class="name">'+this.dataGallery[current_page].title+'</span>';
            jQuery('#popupGallery .wrapPopupGallery .thumbnail').html(output);
            this.index = current_page;
        }

    },
    prev_gallery() {
        console.log(this.index )
        console.log(this.total_el )
        if (this.index > 0 && this.dataGallery.length > 1) {
            let current_page = this.index - 1;
            let page_current = current_page + 1;
            let link = this.dataGallery[current_page].full_image_url;
            let output = '<img src="' + link + '">'
            output += '<i class="flaticon-delete"></i>';
            output += '<span class="index">'+ page_current  +'/'+this.total_el+'</span>';
            output += '<span class="name">'+this.dataGallery[current_page].title+'</span>';
            jQuery('#popupGallery .wrapPopupGallery .thumbnail').html(output);
            this.index = current_page;
        }
    }
}

var slide_home = {
    index: 0,
    total_el:0,
    data: [],
    popup_slide_home(data) {
        // console.log(data);
        jQuery('#popup_slide_home .item_slide_home').html(data.video);
        jQuery('#popup_slide_home ').show();
        this.index = data.index;
        let vm = this
        jQuery.ajax({
            url: ajaxurl,
            type: "POST",
            // dataType : "text",
            data: ({
                action: 'popup_video_home',
                id:data.id,
                name:[],
            }),
            beforeSend: function () {
                // $( '.loading_ajaxp' ).css( 'display','block' );
            },
            success: function (res, textStatus, jqXHR) {
                if (res.success == true) {
                    if (res.data.video) {
                        vm.data= res.data.video;
                        vm.total_el = vm.data.length
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log('The following error occured: ' + textStatus, errorThrown);
            },
            complete: function (jqXHR, textStatus) {
            }
        });
    },
    prev(){
        if (this.index > 0 && this.data.length > 1) {
            let current_page = this.index - 1;
            this.index = current_page;
            let video = this.data[current_page];
            jQuery('#popup_slide_home .item_slide_home').html(video);
        }
    },
    next(){
        if (this.index < this.total_el - 1 && this.data.length > 1) {
            let current_page = this.index + 1;
            this.index = current_page;
            let video = this.data[current_page];
            jQuery('#popup_slide_home .item_slide_home').html(video);
        }
    },
    close_slide_home(event) {
        console.log(event.target.className);
        if (event.target.className != 'inner_popup_slide_home ') {

        } else {
            jQuery('#popup_slide_home ').hide();
            jQuery('#popup_slide_home .item_slide_home').html("");
        }
    }
}