<?php
/**
 * Created by PhpStorm.
 * User: nguye
 * Date: 19/10/2018
 * Time: 1:09 AM
 */
add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {

        show_admin_bar(false);

}
add_action('wp_head', 'myplugin_ajaxurl');

function myplugin_ajaxurl() {

    echo '<script type="text/javascript">
           var ajaxurl = "' . admin_url('admin-ajax.php') . '";
         </script>';
}
//?>