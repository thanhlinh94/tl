<?php
/**
 * Created by PhpStorm.
 * User: nguye
 * Date: 19/10/2018
 * Time: 1:55 AM
 */
/*
 Template Name: home
 */
get_header(); ?>
<div id="fullPageHome">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri() . '/assets/css/home.css' ?>">
    <?php get_template_part('slide', 'home'); ?>
    <?php
    /*
     * Hiển thị danh sách các page trang chủ
     */
    $list_page = get_field('list_id_page');
    if ($list_page) {
        $the_query = new WP_Query(array('post_type' => 'page', 'post__in' => $list_page)); ?>
        <div class="listPageHome">
            <ul class="wrap">
                <?php while ($the_query->have_posts()) :
                    $the_query->the_post(); ?>
                    <li class="itemPage">
                        <div class="wrapItem">
                            <a class="buttonLink" href="<?php the_permalink(); ?>"
                               title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
                            <?php echo the_post_thumbnail('medium_large', ['alt' => get_the_title()]); ?>
                        </div>
                    </li>
                <?php endwhile; ?>
            </ul>
        </div>
        <?php wp_reset_query();
    } ?>
    <?php
    $id_about_us = get_field('id_page_about_us');
    if ($id_about_us) {
        ?>
        <div class="pageAboutUs">
            <div class="wrap">
                <?php $the_query = new WP_Query('page_id= '. $id_about_us); ?>
                <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
                    <div class="content_page">
                        <?php the_content(); ?>
                    </div>
                    <a class="buttonLink" href="<?php the_permalink(); ?>"
                       title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
                <?php endwhile; ?>


            </div>
        </div>
        <?php
        wp_reset_query();
    }
    ?>
</div>
<?php
get_footer();
?>
