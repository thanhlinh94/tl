<?php
/**
 * Created by PhpStorm.
 * User: nguye
 * Date: 20/10/2018
 * Time: 1:16 AM
 */
?>
<section id="slideHome">
    <div class="wrapSlideHome owl-carousel owl-theme">
        <?php
        $list_slide = ['item_slide_1', 'item_slide_2', 'item_slide_3', 'item_slide_4'];
        foreach ($list_slide as $key => $value) {
            $item = (get_field($value));
            if ($item && $item['image']):
                $data = [
                    'index' => $key,
                    'id' =>  get_the_ID(),
                    'video' => $item['video']
                ];
                $data  = json_encode($data );
                ?>
                <div class="itemSlide" onclick='slide_home.popup_slide_home(<?php echo $data; ?>)'>
                    <img src="<?php echo $item['image']['url'] ?>" alt="">
                </div>
            <?php endif;
        } ?>
    </div>
    <div id="popup_slide_home">
        <div class="inner_popup_slide_home " onclick='slide_home.close_slide_home(event)'>
            <div class="item_slide_home">

            </div>
        </div>
        <span onclick="slide_home.prev()" class="prev flaticon-arrowhead-thin-outline-to-the-left"></span>
        <span onclick="slide_home.next()" class="next flaticon-arrow"></span>
    </div>
</section>
<script>
    jQuery(document).ready(function () {
        jQuery('.wrapSlideHome').owlCarousel({
            loop: true,
            nav: true,
            items: 1,
            navText: ["<i class='flaticon-arrowhead-thin-outline-to-the-left'></i>", "<i class='flaticon-arrow'></i>"],
        })
    });

</script>
