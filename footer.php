<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

		</div><!-- .site-content -->
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() . '/assets/css/footer.css' ?>">
		<span class="foo-scroll-top" onclick="topFunction()" id="scrollTop"></span>
		<footer>
			<div id="fb-root"></div>
			<script>(function(d, s, id) {
			  var js, fjs = d.getElementsByTagName(s)[0];
			  if (d.getElementById(id)) return;
			  js = d.createElement(s); js.id = id;
			  js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.2&appId=251772248705168&autoLogAppEvents=1';
			  fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="foo-full">
				<div class="foo-ad-box">
					<div class="container">
						<div class="row">
							<div class="col-md-6">
								<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-top-left') ) : 
										
								endif; ?>
							</div>
							<div class="col-md-6">
								<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-top-right') ) : 
 										
								endif; ?>
							</div>
						</div>
					</div>
				</div>
				<div class="foo-fpage-box">
					<div class="container">
						<div class="foo-wrap-fpage">
							<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('footer-middle') ) : 
 										
							endif; ?>
						</div>
					</div>
				</div>
				<div class="foo-footer">
					<div class="container">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'footer',
								'menu_class'     => 'row menu-footer'
							 ) );
						?>
					</div>
				</div>
			</div>
		</footer>
		<script type="text/javascript" src="<?php echo get_template_directory_uri() . '/assets/js/footer.js' ?>"></script>
	</div><!-- .site-inner -->
</div><!-- .site -->
<?php wp_footer(); ?>
</body>
</html>
