<?php
/*
 Template Name: contact-us
 */
get_header();
 ?>
 <style type="text/css">
 	<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail'); ?>
	<?php endif; ?>
 	.ct-img-bg {
 		background-image: url('<?php echo $image[0]; ?>');
		background-position: center left;
		background-size: cover;
 	}
 	.ct-bg-head {
 		padding-top: 96px;
 	}
 	@media only screen and (max-width: 900px) {
	    .au-bg-head {
        	padding-top: 79px;
	    }
	}
	.ct-bg-title {
		font-size: 50px;
		color: #555;
		font-weight: 700;
		margin: 45px 0 10px;
		text-align: center;
	}
	.ct-bg-section-2 {
		font-size: 20px;
		font-weight: 400;
		color: #555;
		line-height: 1.5em;
		padding-bottom: 25px;
	}
 </style>
 <div class="ct-full">
 	<div class="ct-bg-head ct-img-bg"></div>
 	<div class="container">
 		<div class="ct-gb-section">
			<h1 class="ct-bg-title"><?php echo get_the_title($page->ID); ?></h1>
			<div class="ct-bg-section-2">
				<?php echo get_post_field('post_content', $post->ID); ?>
			</div>
		</div>
 	</div>
 </div>
 <?php
 get_footer();
 ?>