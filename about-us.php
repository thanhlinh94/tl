<?php 
/*
 Template Name: about-us
 */
get_header();
 ?>
<style type="text/css">
	.au-bg-head {
		padding-top: 96px;
		padding-bottom: 150px;
	}
	@media only screen and (max-width: 900px) {
	    .au-bg-head {
	        padding-top: 229px;
	    }
	}
	<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail'); ?>
	<?php endif; ?>
	.au-img-bg {
		background-image: url('<?php echo $image[0]; ?>');
		background-position: center left;
		background-size: cover;
	}
	.au-gb-section {
		background-color: #fff;
		position: relative;
	}
	.au-bg-section-1 {
	    padding-top: 15px 0 2px;
	}
	.au-bg-title {
		font-size: 50px;
		color: #555;
		font-weight: 700;
		margin: 45px 0 45px 0;
	}
	.au-bg-title.tt-bl {
		text-align: center;
	}
	.au-gb-section-2 {
	    padding-top: 15px;
	    padding-right: 0;
	    padding-bottom: 12px;
	    padding-left: 0;
	}
	.au-bg-hr {
		height: 23px;
	}
	.au-divider {
	    margin: 0;
	    padding: 0;
	    border: 0;
	    outline: 0;
	    background: 0 0;
	    font-size: 100%;
	    vertical-align: baseline;
	}
	.au-bg-divider {
		display: inline-block;
		width: 100%;
	}
	.au-bg-line {
		position: relative;
	}
	.au-bg-line:before {
		position: absolute;
	    z-index: 10;
	    top: 0;
	    left: 0;
	    width: 100%;
	    height: 1px;
	    border-top-width: 1px;
	    border-top-style: solid;
	    border-top-color: #eee;
	    border-top-color: rgba(0,0,0,.1);
	    content: "";
	}
	.au-bg-line:after {
	    display: table;
    	content: "";
	}
	.au-gb-section-2 {
		padding: 27px 0;
	}
	.au-wrap-pic {
		display: inline-flex;
		justify-content: center;
	}
	.au-cl-1-2 .au-cl-1 {
		display: flex;
		flex-direction: column;
	}
	.au-pic {
		display: flex;
		justify-content: center;
		align-items: center;
		vertical-align: middle;
		margin-bottom: 15px;
	}
	.au-wrap-pic img {
		width: 100%;
		height: 100%;
	}
	.au-txt {
		font-size: 20px;
		font-weight: 400;
		color: #555;
		line-height: 1.5em;
	}
	.au-cap-txt {
		font-style: italic;
	}
	.au-pic-cl {
		margin-bottom: 15px;
	}
	.au-bg-btn {
		padding: 27px 0 50px;
		display: flex;
		justify-content: center;
		align-items: center;
	}
	.au-txt-href {
		color: #fff;
		background-color: #8e8e8e;
		font-weight: 700;
		font-size: 20px;
		transition: all 0.3s;
	    padding: 10px 20px;
    	line-height: 30px;
		text-decoration: none;
		text-transform: uppercase;
	}
	.au-txt-href:hover {
		background-color: #595959;
	}
</style>
<div class="au-full">
	<div class="au-bg-head au-img-bg"></div>
	<div class="container">
		<div class="au-gb-section au-bg-section-1">
			<h1 class="au-bg-title tt-bl"><?php echo get_the_title($page->ID); ?></h1>
		</div>
		<div class="au-bg-hr au-bg-line">
			<div class="au-bg-divider au-divider"></div>
		</div>
		<div class="au-gb-section au-gb-section-2">
			<div class="row">
				<?php if (get_field('img_ct_1')): ?>
				<div class="col-md-6 au-cl-1-2 au-cl-1">
					<div class="au-pic">
						<?php $img_1 = get_field_object('img_ct_1'); ?>
						<span class="au-wrap-pic">
							<img src="<?php echo $img_1['value']['url']; ?>">
						</span>
					</div>
					<div class="au-pic-cap">
						<span class="au-cap-txt au-txt"><?php echo $img_1['value']['caption']; ?></span>
					</div>
				</div>
				<?php endif; ?>
				<?php if (get_field('content_ct_1')): ?>
				<div class="col-md-6 au-cl-1-2 au-cl-2">
					<span class="au-desc-pic au-txt">
						<?php echo get_field('content_ct_1'); ?>
					</span>
				</div>
				<?php endif; ?>
			</div>
		</div>
		<div class="au-bg-hr au-bg-line">
			<div class="au-bg-divider au-divider"></div>
		</div>
		<div class="au-gb-section au-gb-section-2">
			<div class="row">
				<div class="col-md-6">
					<?php if (get_field('img_ct_2')): ?>
					<div class="au-pic-cl">
						<div class="au-pic">
							<?php $img_2 = get_field_object('img_ct_2'); ?>
							<span class="au-wrap-pic">
								<img src="<?php echo $img_2['value']['url']; ?>">
							</span>
						</div>
						<div class="au-pic-cap">
							<span class="au-cap-txt au-txt"><?php echo $img_2['value']['caption']; ?></span>
						</div>
					</div>
					<?php endif; ?>
					<?php if (get_field('img_ct_3')): ?>
					<div class="au-pic-cl">
						<div class="au-pic">
							<?php $img_3 = get_field_object('img_ct_3'); ?>
							<span class="au-wrap-pic">
								<img src="<?php echo $img_3['value']['url']; ?>">
							</span>
						</div>
						<div class="au-pic-cap">
							<span class="au-cap-txt au-txt"><?php echo $img_3['value']['caption']; ?></span>
						</div>
					</div>
					<?php endif; ?>
				</div>
				<div class="col-md-6">
					<?php if (get_field('img_ct_4')): ?>
					<div class="au-pic-cl">
						<div class="au-pic">
							<?php $img_4 = get_field_object('img_ct_4'); ?>
							<span class="au-wrap-pic">
								<img src="<?php echo $img_4['value']['url']; ?>">
							</span>
						</div>
						<div class="au-pic-cap">
							<span class="au-cap-txt au-txt"><?php echo $img_4['value']['caption']; ?></span>
						</div>
					</div>
					<?php endif; ?>
					<?php if (get_field('img_ct_5')): ?>
					<div class="au-pic-cl">
						<div class="au-pic">
							<?php $img_5 = get_field_object('img_ct_5'); ?>
							<span class="au-wrap-pic">
								<img src="<?php echo $img_5['value']['url']; ?>">
							</span>
						</div>
						<div class="au-pic-cap">
							<span class="au-cap-txt au-txt"><?php echo $img_5['value']['caption']; ?></span>
						</div>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<div class="au-bg-hr au-bg-line">
			<div class="au-bg-divider au-divider"></div>
		</div>
		<div class="au-gb-section au-gb-section-2">
			<div class="row">
				<?php if (get_field('content_ct_2')): ?>
				<div class="col-md-6 au-cl-1-2 au-cl-2">
					<span class="au-desc-pic au-txt">
						<?php echo get_field('content_ct_2'); ?>
					</span>
				</div>
				<?php endif; ?>
				<?php if (get_field('img_ct_6')): ?>
				<div class="col-md-6 au-cl-1-2 au-cl-1">
					<div class="au-pic">
						<?php $img_6 = get_field_object('img_ct_6'); ?>
						<span class="au-wrap-pic">
							<img src="<?php echo $img_6['value']['url']; ?>">
						</span>
					</div>
					<div class="au-pic-cap">
						<span class="au-cap-txt au-txt"><?php echo $img_6['value']['caption']; ?></span>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
		<div class="au-bg-hr au-bg-line">
			<div class="au-bg-divider au-divider"></div>
		</div>
		<?php if (get_field('link_bottom')): ?>
		<div class="au-bg-section au-bg-section-3">
			<div class="au-bg-btn">
				<?php $img_6 = get_field('link_bottom'); ?>
				<a href="<?php echo $img_6['url'] ?>" class="au-txt-href"><?php echo $img_6['title'] ?></a>
			</div>
		</div>
		<?php endif; ?>
	</div>
</div>
 <?php 
get_footer();
 ?>